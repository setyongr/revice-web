<!DOCTYPE html>
<!--
REVICE Admin App
version 1.0
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" data-ng-app="ReviceApp">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <title data-ng-bind="'Revice Web| ' + $state.current.data.pageTitle"></title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="bower_components/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css" />
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="bower_components/jquery-uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="bower_components/angular-loading-bar/build/loading-bar.min.css" rel="stylesheet" type="text/css" />
        <link href="bower_components/ngToast/dist/ngToast.min.css" rel="stylesheet" type="text/css" />
        <link href="bower_components/ngToast/dist/ngToast-animations.css" rel="stylesheet" type="text/css" />

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before" />
        <!-- END DYMANICLY LOADED CSS FILES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="assets/global/css/components.min.css" id="style_components" rel="stylesheet" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout2/css/custom.css" rel="stylesheet" type="text/css" />

        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

    <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo page-on-load" ng-class="{'page-sidebar-closed': settings.layout.pageSidebarClosed}">
        <toast></toast>
        <!-- BEGIN PAGE SPINNER -->
        <div ng-spinner-bar class="page-spinner-bar">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <!-- END PAGE SPINNER -->
        <div ui-view></div>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE JQUERY PLUGINS -->
        <!--[if lt IE 9]>
      	<script src="assets/global/plugins/respond.min.js"></script>
      	<script src="assets/global/plugins/excanvas.min.js"></script>
      	<![endif]-->
        <script src="bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <!-- <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script> -->
        <!-- <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuglouvaHCfA-Tbs28jZ-OmFfM8deEiKU&libraries=places">
    </script> -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="bower_components/blockui/jquery.blockUI.js" type="text/javascript"></script>
        <script src="bower_components/js-cookie/src/js.cookie.js" type="text/javascript"></script>
        <script src="bower_components/jquery-uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- <script src="bower_components/jquery-locationpicker-plugin/dist/locationpicker.jquery.min.js" type="text/javascript"></script> -->
        <script src="bower_components/lodash/dist/lodash.min.js" type="text/javascript"></script>
        <!-- END CORE JQUERY PLUGINS -->
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="bower_components/angular/angular.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-sanitize/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-touch/angular-touch.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="bower_components/ocLazyLoad/dist/ocLazyLoad.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-loading-bar/build/loading-bar.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-animate/angular-animate.min.js" type="text/javascript"></script>
        <script src="bower_components/ngToast/dist/ngToast.min.js" type="text/javascript"></script>
        <script src="bower_components/angular-simple-logger/dist/angular-simple-logger.min.js"type="text/javascript"></script>
        <script src="bower_components/angular-google-maps/dist/angular-google-maps.min.js"type="text/javascript"></script>


        <!--[if lte IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/Base64/0.3.0/base64.min.js"></script>
        <![endif]-->
        <script src="bower_components/satellizer/satellizer.min.js" type="text/javascript"></script>
        <!-- END CORE ANGULARJS PLUGINS -->
        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/directives.js" type="text/javascript"></script>
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout2/scripts/layout.js" type="text/javascript"></script>
        <!-- END APP LEVEL JQUERY SCRIPTS -->
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
