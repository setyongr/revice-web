<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'nama' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'alamat' => $faker->address,
        'telp' => $faker->phoneNumber,
    ];
});

$factory->define(App\Service::class, function(Faker\Generator $faker){
    $longitude = (float) 109.243693;
    $latitude = (float) -7.423253;

    $rd = 10000 / 111300;

    $u = (float)mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();
    $v = (float)mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();

    $w = (float)($rd * sqrt($u));
    $t = 2 * pi() * $v;
    $x = $w * cos($latitude);
    $y = $w * sin($longitude);
    $xp = $x / cos($latitude);

    $nlat = $y + $latitude;
    $nlng = $xp + $longitude;



    return [
        'nama' => $faker->name,
        'alamat' => $faker->address,
        'deskripsi' => $faker->sentence(6),
        'telp' => $faker->phoneNumber,
        'tipe_id' => $faker->numberBetween(1, 3),
        'lat' => $nlat,
        'lng' => $nlng
    ];
});
