<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TipeSeeder::class);
        // $this->call(ServiceSeeder::class);
        Model::reguard();
    }
}
//
