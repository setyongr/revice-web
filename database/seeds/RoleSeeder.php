<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        Role::create(['name'=>'admin', 'description' => 'Manage all data']);
        Role::create(['name'=>'user', 'description' => 'Normal User']);
        Role::create(['name'=>'pemilik', 'description' => 'Pemilik Service']);
    }
}
