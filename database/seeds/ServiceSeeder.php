<?php

use Illuminate\Database\Seeder;

use App\Service;
class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('services')->delete();
      factory(App\Service::class, 20)->create()->each(function($s){
        $s->admins()->attach(rand(1, 3));
      });
    }
}
