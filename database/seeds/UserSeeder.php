<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();

      $users = array(
            ['nama' => 'admin', 'email' => 'admin@revice.io', 'password' => 'secret', 'alamat' => 'Jl.No 2', 'telp' => '087676767'],
             ['nama' => 'user', 'email' => 'normal@revice.io', 'password' => 'secret', 'alamat' => 'Jl.No 2', 'telp' => '087676767'],
             ['nama' => 'pemilik', 'email' => 'pemilik@revice.io', 'password' => 'secret', 'alamat' => 'Jl.No 2', 'telp' => '087676767'],
      );

      foreach ($users as $user) {
        User::create($user)->setRole($user['nama']);
      }

    //   factory(App\User::class, 30)->create()->each(function($s){
    //      $s->setRole('user');
    //   });
    }
}
