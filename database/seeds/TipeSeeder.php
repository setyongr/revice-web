<?php

use Illuminate\Database\Seeder;

use App\Tipe;
class TipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipes')->delete();
        Tipe::create(['nama' => 'Tambal Ban', 'deskripsi' => 'Bengkel Tambal Ban' ]);
        Tipe::create(['nama' => 'Bengkel', 'deskripsi' => 'Bengkel Servis']);
        Tipe::create(['nama' => 'Pom Bensin', 'deskripsi' => 'Pom Bensin']);
    }
}
