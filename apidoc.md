FORMAT: 1A

# Example

# AppHttpControllersApiAuthenticateController

# Password Reset [/auth/reset]
Password Reset Resource

## Request Password Reset Email [POST /auth/reset]


+ Request (application/x-www-form-urlencoded)
    + Body

            email=foo@bar.baz

+ Response 200 (application/json)
    + Body

            {
                "id": 10
            }

# AppHttpControllersApiUserController