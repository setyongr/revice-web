<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $table = 'services';
    protected $fillable = ['nama','alamat', 'deskripsi', 'telp', 'tipe_id', 'lat', 'lng'];
    protected $hidden = ['tipe', 'admins'];
     protected $appends = ['tipe_name', 'admin_id', 'admin_name'];

    public function admins(){
        return $this->belongsToMany('App\User', 'serviceadmins');
    }

    public function tipe(){
        return $this->belongsTo('App\Tipe');
    }
    public function getAdminNameAttribute(){
        //FIX ME: solusi asu, anu angel php ne crash bae
        $id = $this->admins->lists('id');
        $nama = $this->admins->lists('nama');

        $ret = array();
        for ($i=0; $i < sizeof($id); $i++) {
            $tmp["id"] = $id[$i];
            $tmp["nama"] = $nama[$i];
            array_push($ret, $tmp);
        }

        return $ret;
    }

    public function getAdminIdAttribute(){
        return $this->admins->lists('id');
    }

    public function getTipeNameAttribute(){
        return $this->tipe->nama;
    }
}
