<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCompleted extends Model
{
  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = 'order_completed';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['order_id', 'service_id'];

  public function order(){
    return $this->belongTo('App\Order');
  }

}
