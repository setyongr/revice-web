<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next)
  {
    $this->request = $request;
    // if route has access
    if (! $this->getAction('is') or $this->hasRole()) {
        return $next($request);
    }

    return response()->json([
        'message' => 'You are not authorized to access this resource.'], 401);
  }
  /**
   * Check if user has requested route role.
   *
   * @return bool
   */
  protected function hasRole()
  {
      $request = $this->request;
      $role = $this->getAction('is');

      return $request->user()->is($role);
  }

  /**
   * Extract required action from requested route.
   *
   * @param string $key action name
   * @return string
   */
  protected function getAction($key)
  {
      $action = $this->request->route()->getAction();

      return isset($action[$key]) ? $action[$key] : false;
  }

}
