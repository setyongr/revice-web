<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/test', 'Api\UserController@test');

/**
* API Route Group
*/
DingoRoute::version('v1', function ($api) {

    $api->post('test', 'App\Http\Controllers\Api\NotificationController@newOrder');
    //Authentication module
    $api->post('auth', 'App\Http\Controllers\Api\AuthenticateController@authenticate');
    $api->post('admin/auth', 'App\Http\Controllers\Api\AuthenticateController@authenticateAdmin');

    //Password reset module
    $api->post('auth/reset', 'App\Http\Controllers\Api\PasswordController@reset');
    $api->post('auth/reset/confirm', 'App\Http\Controllers\Api\PasswordController@confirmreset');

    /**
    * User Module
    */
    $api->group(['prefix' => 'user'], function($api){
        //User Method that need authentication
        $api->group(['middleware' => ['api.auth','role']], function($api){
            $api->get('/', [
                'uses' => 'App\Http\Controllers\Api\UserController@index',
                'is' => 'admin'
            ]);

            $api->post('/supercreate', [
                'uses' => 'App\Http\Controllers\Api\UserController@supercreate',
                'is' => 'admin'
            ]);

            $api->put('/token', [
                'uses' => 'App\Http\Controllers\Api\UserController@settoken'
            ]);

            $api->group(['prefix' => '/{id}'], function($api){
                $api->get('/', [
                    'uses' => 'App\Http\Controllers\Api\UserController@show',
                ]);
                $api->put('/', [
                    'uses' => 'App\Http\Controllers\Api\UserController@update',
                ]);
                $api->delete('/', [
                    'uses' => 'App\Http\Controllers\Api\UserController@destroy',
                    'is' => 'admin'
                ]);


            });
        });

        //Method that doesn't need authentication
        $api->post('/create', 'App\Http\Controllers\Api\UserController@create');
        $api->get('/confirm/{token}', 'App\Http\Controllers\Api\UserController@confirm');
    });

    $api->group(['prefix' => 'service'], function($api){
        // Service need auth
        $api->group(['middleware' => ['api.auth', 'role']], function($api){

            $api->group(['prefix' => '/{serviceId}', 'is'=>'admin'], function($api){
                $api->put('/', [
                    'uses' => 'App\Http\Controllers\Api\ServiceController@update',
                ]);
                $api->delete('/', [
                    'uses' => 'App\Http\Controllers\Api\ServiceController@destroy',
                ]);

                //Service manager
                $api->group(['prefix' => '/manager'], function($api){
                    $api->get('/', 'App\Http\Controllers\Api\ServiceController@getManager');
                    $api->post('/', 'App\Http\Controllers\Api\ServiceController@postManager');
                    $api->delete('/{userId}', 'App\Http\Controllers\Api\ServiceController@deleteManager');
                });
            });

            $api->post('/', 'App\Http\Controllers\Api\ServiceController@create');
        });

        // FIXED
        $api->get('/', [
            'uses' => 'App\Http\Controllers\Api\ServiceController@index'
        ]);

        // FIXED
        $api->get('/closest', [
            'uses' => 'App\Http\Controllers\Api\ServiceController@closest'
        ]);

        // FIXED
        $api->get('/{id}', [
            'uses' => 'App\Http\Controllers\Api\ServiceController@show'
        ]);
    });

    $api->group(['prefix' => 'tipe', 'middleware' => ['api.auth', 'role']], function($api){
        $api->get('/', [
            'uses' => 'App\Http\Controllers\Api\TipeController@index'
        ]);

        // $api->get('/{id}', )
    });

    // Order Management
    $api->group(['prefix' => 'order', 'middleware' => ['api.auth', 'role']], function($api){
        $api->get('/{id}', [
            'uses' => 'App\Http\Controllers\Api\OrderController@show'
        ]);

        $api->post('/create', [
            'uses' => 'App\Http\Controllers\Api\OrderController@create'
        ]);

        $api->put('/status/{id}', [
            'uses' => 'App\Http\Controllers\Api\OrderController@update'
        ]);
    });

});
