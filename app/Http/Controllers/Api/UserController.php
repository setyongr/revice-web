<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Mailers\AppMailer;
use App\User;
use Hash;
use Validator;

class UserController extends Controller
{

    /**
    * Display a listing of the user.
    * @param \Illuminate\Http\Request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if($request->limit)
        return User::paginate($request->limit);
        else
        return User::paginate(10);
    }

    /**
    * Create new user.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request, AppMailer $mail)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'nama' => 'required',
            'password' => 'required',
            'alamat'  => 'required',
            'telp'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'could not create new user.', $validator->errors());
        }
        $user = User::create($request->all());
        $user->setRole('user');
        $mail->sendEmailConfirmationTo($user);

        return $user;
    }

    /**
    * Create new user without email confirmation.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function supercreate(Request $request, AppMailer $mail)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'nama' => 'required',
            'password' => 'required',
            'alamat'  => 'required',
            'telp'  => 'required',
            'verified'  => 'sometimes|required',
            'role_name'  => 'sometimes|required|exists:roles,name'

        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'could not create new user.', $validator->errors());
        }
        $user = User::create($request->all());
        $user->setRole($request->role_name);

        return $user;
    }


    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $id)
    {
        if ($id == "current"){
            $id = $request->user()->id;
        }else if(!$request->user()->is('admin')){
            return abort(401, 'You are not authorized to access this resource.');
        }

        if (!$user = User::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'User not found');
        }

        return $user;

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        if ($id == "current"){
            $id = $request->user()->id;
        }else if(!$request->user()->is('admin')){
            return abort(401, 'You are not authorized to access this resource.');
        }

        if (!$user = User::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'User not found');
        }

        // Validate input
        $rules = [
            'email' => 'sometimes|required|email|unique:users,email,'.$id,
            'nama' => 'sometimes|required',
            'password' => 'sometimes|required',
            'alamat'  => 'sometimes|required',
            'telp'  => 'sometimes|required',
            'verified'  => 'sometimes|required',
            'role_name'  => 'sometimes|required|exists:roles,name'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'could_not_update_user.', $validator->errors());
        }
        // Update the user
        $user->update($request->all());
        if($request->user()->is('admin') && $request->role_name){ //Check if admon and role field exist
            $user->setRole($request->role_name);
        }
        return $user;
    }


        /**
        * Set token to the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function settoken(Request $request)
        {
            $user = $request->user();

            // Validate input
            $rules = [
                'token'  => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                throw new \Dingo\Api\Exception\StoreResourceFailedException(
                'could_not_set_token.', $validator->errors());
            }
            // Update the user
            $user->update(['gcmtoken' => $request->token]);
            return $user;
        }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        if (!$user = User::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'User not found');
        }

        $user->delete();

        return response()->json(
        ['message'=>'User '. '$id' . ' successfully deleted'],
        200);
    }

    /**
    * Confirm user registration email
    *
    * @param String $token
    * @return \Illuminate\Http\Response
    */
    public function confirm($token)
    {
        if(!$user = User::whereToken($token)->first()){
            throw new \Dingo\Api\Exception\UpdateResourceFailedException(
            'Could not confirm email');
        }
        $user->confirmEmail();
        return response()->json(['message'=>'email_cofirmed'], 200);
    }
}
