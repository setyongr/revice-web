<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\ServiceRepository;

use App\Service;
use Validator;

class ServiceController extends Controller
{
    protected $service;
    public function __construct(ServiceRepository $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        if($request->limit)
        return Service::paginate($request->limit);
        else
        return Service::paginate(10);
    }
    /**
    * Create new service.
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'alamat' => 'required',
            'deskripsi' => 'required',
            'telp'  => 'required',
            'tipe_id' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'could not create new service.', $validator->errors());
        }
        $service = Service::create($request->all());

        return $service;
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $id)
    {
        if (!$service = Service::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'Service not found');
        }

        return $service;

    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        if (!$service = Service::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
            ('Service not found');
        }

        $rules = [
            'nama' => 'sometimes|required',
            'alamat' => 'sometimes|required',
            'deskripsi' => 'sometimes|required',
            'telp'  => 'sometimes|required',
            'tipe_id' => 'sometimes|required',
            'lat' => 'sometimes|required',
            'lng' => 'sometimes|required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'Could not update service.', $validator->errors());
        }
        // Update the user
        $service->update($request->all());
        return $service;
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        if (!$service = Service::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'Service not found');
        }

        $service->delete();

        return response()->json(
        ['message'=>'Service '. '$id' . ' successfully deleted'],
        200);
    }

    public function closest(Request $request){

        $rules = [
            'lat' => 'required',
            'lng' => 'required',
            'dist' => 'required',
            'max'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'Could not get service.', $validator->errors());
        }
        $closest = $this->service->closest($request->lat, $request->lng, $request->dist, $request->max, "kilometers");

        $services = array(
            'total' => sizeof($closest),
            'data'  => $closest
        );

        return $services;
    }


    // Manager
    public function getManager($id)
    {
        if (!$service = Service::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'Service not found');
        }

        return $service->admins;
    }

    public function postManager(Request $request, $id)
    {
        if (!$service = Service::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'Service not found');
        }
        $rules = [
            'userId' => 'required|exists:users,id',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'Could not update service.', $validator->errors());
        }
        $service->admins()->attach($request->userId);
        return $service;
    }

    public function deleteManager(Request $request,$id,$userId)
    {
        if (!$service = Service::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'Service not found');
        }


        $service->admins()->detach($request->userId);
        return $service;
    }

}
