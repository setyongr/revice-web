<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Order;
use App\OrderCompleted;
use App\Service;
use App\Repositories\NotificationRepository;
use App\Repositories\ServiceRepository;


class OrderController extends Controller
{

    protected $notif;
    protected $service;

    //CONTANT VARIABLE
    const STATUS_PENDING = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_CANCELLED = 2;

    const TYPE_NEWORDER = 0;
    const TYPE_UPDATEORDER = 1;


    public function __construct(NotificationRepository $notif, ServiceRepository $service)
    {
        $this->notif = $notif;
        $this->service = $service;

    }
    public function create(Request $request){
        $rules = [
            'type' => 'required',
            'desc' => 'required',
            'location' => 'required',
            'lat' => 'required',
            'lng'  => 'required',
            'dist' => 'required',
            'max'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'could not create new order.', $validator->errors());
        }

        $closest = $this->service->closest($request->lat, $request->lng, $request->dist, $request->max, "kilometers");
        // return $closest;
        $receiver = $closest->implode('id', ',');

        $order = Order::create([
            'problem_type' => $request->type,
            'problem_desc' => $request->desc,
            'location_desc' => $request->location,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'user_id' => $request->user()->id,
            'receiver' => $receiver,
            'status' => Self::STATUS_PENDING
        ]);

        //Notify


        $gcmData = [
            'type' => Self::TYPE_NEWORDER,
            'id' => $order->id,
            'user_id' => $order->user_id,
            'user_name' => $order->user->nama,
            'problem_type' => $order->problem_type,
            'status' => $order->status,
            'datetime' => $order->created_at->toDateTimeString()];
        $orderArray = $order->toArray();
        if($receiver != ""){
            $res = $this->notif->PushServiceAdmin(explode(',', $receiver), $gcmData);
            // return $res;
            $orderArray['gcm_msg'] = json_decode($res->getBody());
        }
        return ['order' => $orderArray];
    }

    public function show(Request $request, $id){
        if (!$order = Order::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException(
            'Order not found');
        }

        return $order;
    }

    public function update(Request $request, $id){
        if (!$order = Order::find($id)){
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
            ('Order not found');
        }

        $rules = [
            'status'=>'required',
            'service_id' => 'sometimes'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'Could not update order.', $validator->errors());
        }
        $gcmData = [
            'type' => Self::TYPE_UPDATEORDER,
            'id' => $order->id
        ];

        // Update the order
        $order->update(['status' => $request->status]);
        if($request->status == self::STATUS_PROCESSED){
            $order->completed()->save(new OrderCompleted(['service_id' => $request->service_id]));
            $service = Service::find($order->service_id);
            $gcmData['status'] = $order->status;
            $gcmData['service_id'] = $order->service_id;
            $gcmData['service_name'] = $service->nama;
            $gcmData['datetime'] = $order->updated_at->toDateTimeString();
        }

        if($request->status == self::STATUS_CANCELLED){
            $gcmData['status'] = $order->status;
            $gcmData['datetime'] = $order->updated_at->toDateTimeString();
        }



        // return $gcmData;
        $orderArray = $order->toArray();
        $res = $this->notif->PushServiceAdmin(explode(',', $order->receiver), $gcmData);
        $orderArray['gcm_service_msg'] = json_decode($res->getBody());

        // gcm to sender
        $res = $this->notif->PushToUser($order->user_id, $gcmData);
        $orderArray['gcm_user_msg'] = json_decode($res->getBody());
        return ['order' => $orderArray];

    }
}
