<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Auth;

class AuthenticateController extends Controller
{
  /**
  * Authenticate User
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function authenticate(Request $request)
  {
    $credentials = $request->only('email', 'password');
    try{
      //Verify the credentials and create token for user
      if(!$token = JWTAuth::attempt($credentials)){
        return response()->json(['error'=>'invalid_credentials'], 401);
      }
    }catch(JWTException $e){
      //Something wrong
      return response()->json(['error' => 'could_not_create_token'], 500);
    }
    //if no error
    return response()->json(compact('token'));
  }
  public function authenticateAdmin(Request $request)
  {
    $credentials = $request->only('email', 'password');
    try{
      $user = User::where('email', $credentials['email'])->firstOrFail();

      if(!$user->is('admin')){
          return response()->json(['error'=>'invalid_credentials'], 401);
      }
      //Verify the credentials and create token for user
      if(!$token = JWTAuth::attempt($credentials)){
        return response()->json(['error'=>'invalid_credentials'], 401);
      }
    }catch(JWTException $e){
      //Something wrong
      return response()->json(['error' => 'could_not_create_token'], 500);
    }catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
      return response()->json(['error'=>'invalid_credentials'], 401);
    }
    //if no error
    return response()->json(compact('token'));
  }
}
