<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Dingo\Api\Routing\Helpers;

/**
 * Password Reset Resource
 * @Resource("Password Reset", uri="/auth/reset")
 */
class PasswordController extends Controller
{
  use Helpers;

  /**
  * Request Password Reset Email
  *
  * @Post("/")
  * @Version({"v1"})
  * @Transaction({
  * 	@Request("email=foo@bar.baz", contentType="application/x-www-form-urlencoded"),
  *  @Response(200, body={'message':'Email Sent'})
  *  @Response(422, body=
  *  	{
  *  		'message':'Could not reset user password.',
  *  		'errors' : {
  *  			['field':'reason']
  *  		}
  *  	})
  *  })
  */
  public function reset(Request $request){
    $validator = Validator::make($request->all(),
      ['email' => 'required|email']);
    if($validator->fails()){
      throw new \Dingo\Api\Exception\StoreResourceFailedException(
      'Could not reset user password.', $validator->errors());
    }

    $response = Password::sendResetLink($request->only('email'), function (Message $message) {
        $message->subject($this->getEmailSubject());
    });

    switch ($response) {
        case Password::RESET_LINK_SENT:
            return response()->json(
              ['message'=> trans($response)],
              200);

        case Password::INVALID_USER:
          throw new \Dingo\Api\Exception\StoreResourceFailedException(
            'Could not reset user password.', ['email' => trans($response)]);
    }
  }

  /**
   * Get the e-mail subject line to be used for the reset link email.
   *
   * @return string
   */
  protected function getEmailSubject()
  {
      return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
  }

  /**
   * Reset the given user
   *
   * @Post("/confirm")
   * @Version({"v1"})
   * @Transaction({
   * 	@Request("email=foo@bar.baz&token=foo&password=pass&password_confirmation=pass",
   * 			contentType="application/x-www-form-urlencoded"),
   *  @Response(200, body={'message':'Email Sent'})
   *  @Response(422, body=
   *  	{
   *  		'message':'Could not reset user password.',
   *  		'errors' : {
   *  			['field':'reason']
   *  		}
   *  	})
   *  })
   */
  public function confirmreset(Request $request)
  {
    $rule = [
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed|min:6',
    ];
    $validator = Validator::make($request->all(), $rule);
    if($validator->fails()){
      throw new \Dingo\Api\Exception\StoreResourceFailedException(
        'could not reset user password.', $validator->errors());
    }

      $credentials = $request->only(
          'email', 'password', 'password_confirmation', 'token'
      );

      $response = Password::reset($credentials, function ($user, $password) {
          $this->resetPassword($user, $password);
      });

      switch ($response) {
          case Password::PASSWORD_RESET:
              return response()->json(['message'=>trans($response)], 200);
          default:
              return response()->json(['message' => trans($response)], 400);
      }
  }
  /**
   * Reset the given user's password.
   *
   * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
   * @param  string  $password
   * @return void
   */
  protected function resetPassword($user, $password)
  {
      $user->password = bcrypt($password);

      $user->save();

      Auth::login($user);
  }
}
