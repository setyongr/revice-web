<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'orders';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['problem_type','problem_desc', 'location_desc','user_id', 'lat', 'lng', 'status', 'receiver'];
    protected $hidden = ['user'];
    protected $appends = ['user_data', 'service_id'];

    public function completed(){
      return $this->hasOne('App\OrderCompleted');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getServiceIdAttribute(){
        if(!is_null($this->completed)){
            return $this->completed->service_id;
        }else{
            return null;
        }
    }
    public function getUserDataAttribute(){
        // Only return the data needed
        $user = $this->user;
        $ret = ['nama' => $user->nama, 'alamat' => $user->alamat, 'telp' => $user->telp];
        return $ret;
    }
}
