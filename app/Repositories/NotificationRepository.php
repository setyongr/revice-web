<?php
namespace App\Repositories;
use GuzzleHttp\Client;
use App\Repositories\ServiceRepository;
use App\User;
/**
* Notification
*/
class NotificationRepository
{
    private static $authKey = "AIzaSyDWMFMDWAfvm8TM1lggeQilF3Pdy65Uga8";
    public static function PushChannel($channelName, $data){
        $client = new Client([
            'base_uri' => 'https://gcm-http.googleapis.com/gcm/'
        ]);

        $response = $client->request('POST', 'send', [
            'headers' => [
                'Authorization' => 'key=' . self::$authKey
            ],
            'json' => [
                'to' => '/topics/'.$channelName,
                'data' => $data
            ]
        ]);

        return $response;
    }

    public static function PushServiceAdmin($service, $data){
        $token = array();
        foreach ($service as $s) {
            $gcm = User::find($s)->gcmtoken;
            if(isset($gcm) && !empty($gcm)){
                array_push($token, $gcm);
            }
        }

        // return $token;

        $client = new Client([
            'base_uri' => 'https://gcm-http.googleapis.com/gcm/'
        ]);

        $response = $client->request('POST', 'send', [
            'headers' => [
                'Authorization' => 'key=' . self::$authKey
            ],
            'json' => [
                'registration_ids' => $token,
                'data' => $data
            ]
        ]);
        return $response;
    }

    public static function PushToUser($id, $data){
        $token = array();
        $gcm = User::find($id)->gcmtoken;
        if(isset($gcm) && !empty($gcm)){
            array_push($token, $gcm);
        }


        $client = new Client([
            'base_uri' => 'https://gcm-http.googleapis.com/gcm/'
        ]);

        $response = $client->request('POST', 'send', [
            'headers' => [
                'Authorization' => 'key=' . self::$authKey
            ],
            'json' => [
                'registration_ids' => $token,
                'data' => $data
            ]
        ]);
        return $response;
    }


}
