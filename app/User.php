<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Role;

class User extends Model implements AuthenticatableContract,
CanResetPasswordContract
{
  use Authenticatable, CanResetPassword;

  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = 'users';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['nama', 'email', 'password', 'alamat', 'telp', 'verified', 'gcmtoken'];

  /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
  protected $hidden = ['password', 'remember_token', 'token', 'role_id', 'role', 'services'];

  /**
  * The attributes that appended to model's JSON form
  * @var array
  */

  protected $appends = ['role_name', 'managed_service'];
  /**
  * Boot the model.
  *
  * @return void
  */
  public static function boot()
  {
    parent::boot();

    static::creating(function ($user) {
      $user->token = str_random(30);
    });
  }

  /**
  * Set the password attribute.
  *
  * @param string $password
  */
  public function setPasswordAttribute($password)
  {
      if($password!=''){
          $this->attributes['password'] = bcrypt($password);
      }
  }

  /**
  * Confirm the user.
  *
  * @return void
  */
  public function confirmEmail()
  {
    $this->verified = true;
    $this->token = null;

    $this->save();
  }

  /**
  * Many to Many relationship to Service model
  * @return [type] [description]
  */
  public function services(){
    return $this->belongsToMany('App\Service', 'serviceadmins');
  }

  public function getManagedServiceAttribute(){
      return $this->services;
  }

  public function orders(){
      return $this->hasMany('App\Order');
  }
  /**
  * One to Many relationship to Role model
  * @return [type] [description]
  */
  public function role(){
    return $this->belongsTo('App\Role');
  }

  /**
  * Get assigned role name
  * @return [type] [description]
  */
  public function getRoleNameAttribute(){
    return $this->role->name;
  }
  /**
  * Checks if the user has the given role.
  *
  * @param  string $name
  * @return bool
  */
  public function is($name, $operator = null)
  {
    $operator = is_null($operator) ? $this->parseOperator($name) : $operator;

    $role = $this->role;
    $name = $this->hasDelimiterToArray($name);

    // array of name
    if ( is_array($name) ) {

      if ( ! in_array($operator, ['and', 'or']) ) {
        $e = 'Invalid operator, available operators are "and", "or".';
        throw new \InvalidArgumentException($e);
      }

      $call = 'isWith' . ucwords($operator);

      return $this->$call($name, $role);
    }

    // single name
    return $name == $role->name;
  }

  /**
  * Set Role based on name
  * @param [type] $name [description]
  */
  public function setRole($name){
    if(!$role = Role::where('name', $name)->first()){
      throw new \InvalidArgumentException('Specified role ' . $key . ' does not exists.');
    }
    $this->role_id = $role->id;
    $this->save();
  }


  /*
  |----------------------------------------------------------------------
  | Protected Methods
  |----------------------------------------------------------------------
  |
  */
  protected function parseOperator($str)
  {
    // if its an array lets use
    // and operator by default
    if ( is_array($str) ) {
      $str = implode(',', $str);
    }

    if ( preg_match('/([,|])(?:\s+)?/', $str, $m) ) {
      return $m[1] == '|' ? 'or' : 'and';
    }

    return false;
  }
  /**
  * Converts strings having comma
  * or pipe to an array in
  * lowercase
  *
  * @param $str
  * @return array
  */
  protected function hasDelimiterToArray($str)
  {
    if ( is_string($str) && preg_match('/[,|]/is', $str) ) {
      return preg_split('/ ?[,|] ?/', strtolower($str));
    }

    return is_array($str) ?
    array_filter($str, 'strtolower') : is_object($str) ?
    $str : strtolower($str);
  }
  /**
  * @param $name
  * @param $role
  * @return bool
  */
  protected function isWithAnd($name, $role)
  {
    foreach ($name as $check) {
      if ( $check != $role->name) {
        return false;
      }
    }

    return true;
  }

  /**
  * @param $name
  * @param $role
  * @return bool
  */
  protected function isWithOr($name, $role)
  {
    foreach ($name as $check) {
      if ($name == $role->name){
        return true;
      }
    }

    return false;
  }

  /*
  |----------------------------------------------------------------------
  | Magic Methods
  |----------------------------------------------------------------------
  |
  */

  /**
  * Magic __call method to handle dynamic methods.
  *
  * @param  string $method
  * @param  array  $arguments
  * @return mixed
  */
  public function __call($method, $arguments)
  {
    // Handle isRoleName() methods
    if ( starts_with($method, 'is') and $method !== 'is' and ! starts_with($method, 'isWith') ) {
      $role = substr($method, 2);

      return $this->is($role);
    }

    return parent::__call($method, $arguments);
  }
}
