angular.module('ReviceApp').controller('AuthController', function($auth, $state, $http, $rootScope){
  var vm = this;
  vm.loginError = false;

  vm.login = function(){
    var credentials = {
      email : vm.email,
      password : vm.password
    };

    $auth.login(credentials).then(function(){
      vm.loginError = false;
      return $http.get('api/user/current');
    }, function(error){
      vm.loginError = true;
      vm.errorText = 'Email atau password salah';
    })
    .then(function(response){
      if(!vm.loginError){
        var user = angular.toJson(response.data.user);
        localStorage.setItem('user', user);
        $rootScope.authenticated = true;
        $rootScope.currentUser = user;
        $state.go('app.dashboard');
      }
    })
  };

  vm.hideError = function(){
    vm.loginError = false;
  }
});
