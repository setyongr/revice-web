angular.module('ReviceApp').controller('UserManagementNewController', function($state, $stateParams, $scope, $rootScope, userFactory) {
  $scope.$on('$viewContentLoaded', function() {
    // initialize core components
    App.initAjax();
    Layout.init();
  });

  // set default layout mode
  $rootScope.settings.layout.pageContentWhite = true;
  $rootScope.settings.layout.pageBodySolid = false;
  $rootScope.settings.layout.pageSidebarClosed = false;
  var vm = this;
  vm.user = {
    verified: '0',
    role_name: 'user'
  };

  vm.save = function(){
    userFactory.newUser(vm.user).then(function(response){
      $state.go('app.userManagement.main');
    });
  }
});
