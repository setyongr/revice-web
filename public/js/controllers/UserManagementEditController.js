angular.module('ReviceApp').controller('UserManagementEditController', function($state, $stateParams, $scope, $rootScope, userFactory) {
  $scope.$on('$viewContentLoaded', function() {
    // initialize core components
    App.initAjax();
    Layout.init();
  });

  // set default layout mode
  $rootScope.settings.layout.pageContentWhite = true;
  $rootScope.settings.layout.pageBodySolid = false;
  $rootScope.settings.layout.pageSidebarClosed = false;
  var vm = this;
  vm.user = {};
  vm.password = {};
  userFactory.getUser($stateParams.userId).then(function(response){
    vm.user = response.data.user;
  });

  vm.savePersonal = function(){
    userFactory.saveEdit(vm.user.id, vm.user).then(function(response){
      $state.go('app.userManagement.main');
    });
  };

  vm.savePassword = function(){
    userFactory.saveEdit(vm.user.id, vm.password).then(function(response){
      $state.go('app.userManagement.main');
    });
  }
});
