angular.module('ReviceApp').controller('ServiceManagementController', function($uibModal, $state, $scope, $rootScope, $http, serviceFactory) {
  $scope.$on('$viewContentLoaded', function() {
    // initialize core components
    App.initAjax();
    Layout.init();
  });


  $scope.$on('$stateChangeStart', function(event, toState) {
    if (toState.name === 'app.serviceManagement.main'){
      vm.getServiceList();
    }
  });

    // set default layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    var vm = this;
    vm.services;
    vm.pageNumber = 1;
    vm.count = 10;
    vm.pageTotal = 0;
    vm.getServiceList = function() {
      // TODO: Tambahna error handling
      serviceFactory.getServiceList(vm.pageNumber, vm.count).then(function(response) {
        vm.services = response.data.data;
        vm.pageTotal = response.data.last_page;
      });
    };

    if ($state.is('app.serviceManagement.main')){
      vm.getServiceList();
    };

    vm.nextPage = function() {
      if (vm.pageNumber < vm.pageTotal) {
        vm.pageNumber++;
        vm.getServiceList();
      }
    };

    vm.prevPage = function() {
      if (vm.pageNumber > 1) {
        vm.pageNumber--;
        vm.getServiceList();
      }
    };

    vm.delete = function(serviceId){
      var modal = $uibModal.open({
        animation: true,
        templateUrl: 'myModalContent.html',
        controller: function($scope, $uibModalInstance){
            $scope.ok = function() {
              $uibModalInstance.close('ok');
            }

            $scope.cancel = function(){
              $uibModalInstance.dismiss('cancel');
            }
        }
      });

      modal.result.then(function(status){
        if (status==='ok'){
          serviceFactory.delete(serviceId).then(function(response){
            vm.getServiceList();
          });
        }
      });
    }
  });
