angular.module('ReviceApp').controller('UserManagementController', function($uibModal, $state, $scope, $rootScope, $http, userFactory) {
  $scope.$on('$viewContentLoaded', function() {
    // initialize core components
    App.initAjax();
    Layout.init();
  });

  $scope.$on('$stateChangeStart', function(event, toState) {
    if (toState.name === 'app.userManagement.main'){
      vm.getUserList();
    }
  });

  // set default layout mode
  $rootScope.settings.layout.pageContentWhite = true;
  $rootScope.settings.layout.pageBodySolid = false;
  $rootScope.settings.layout.pageSidebarClosed = false;
  var vm = this;
  vm.users;
  vm.pageNumber = 1;
  vm.count = 10;
  vm.pageTotal = 0;

  vm.getUserList = function() {
    // TODO: Tambahna error handling
    userFactory.getUserList(vm.pageNumber, vm.count).then(function(response) {
      vm.users = response.data.data;
      vm.pageTotal = response.data.last_page;
    });
  };

  if ($state.is('app.userManagement.main')){
    vm.getUserList();
  };

  vm.nextPage = function() {
    if (vm.pageNumber < vm.pageTotal) {
      vm.pageNumber++;
      vm.getUserList();
    }
  };

  vm.prevPage = function() {
    if (vm.pageNumber > 1) {
      vm.pageNumber--;
      vm.getUserList();
    }
  };

  vm.delete = function(userId){
    var modal = $uibModal.open({
      animation: true,
      templateUrl: 'myModalContent.html',
      controller: function($scope, $uibModalInstance){
          $scope.ok = function() {
            $uibModalInstance.close('ok');
          }

          $scope.cancel = function(){
            $uibModalInstance.dismiss('cancel');
          }
      }
    });

    modal.result.then(function(status){
      if (status==='ok'){
        userFactory.delete(userId).then(function(response){
          vm.getUserList();
        });
      }
    });
  }



});
