angular.module('ReviceApp').controller('ServiceManagementNewController',
    function(ngToast, $state, $stateParams, $scope, $rootScope, serviceFactory, uiGmapGoogleMapApi) {
        $scope.$on('$viewContentLoaded', function() {
            // initialize core components
            App.initAjax();
            Layout.init();
        });

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
        var vm = this;
        vm.service = {};
        vm.location = {
            latitude: 0,
            longitude: 0
        };
        vm.save = function() {
            serviceFactory.newService(vm.service).then(function(response) {
                $state.go('app.serviceManagement.main');
                ngToast.create("Berhasil Menambah Service");
            });
        };
        var marker = {
            id: 1,
            coords: {
                latitude: 0,
                longitude: 0
            },
            options: {
                draggable: true
            },
            events: {
                dragend: function(marker, eventName, args) {
                    var lat = marker.getPosition().lat();
                    var lon = marker.getPosition().lng();
                    //update data
                    vm.service.lat = lat;
                    vm.service.lng = lon;
                }
            }
        };


        $scope.marker = marker;

        uiGmapGoogleMapApi.then(function(maps) {
            maps.visualRefresh = true;
        });

        angular.extend($scope, {
            map: {
                control: {},
                center: {
                    latitude: 0,
                    longitude: 0
                },
                zoom: 12,
                dragging: false,
                bounds: {},
                markers: [],
                idkey: 'place_id',
                events: {
                    idle: function(map) {

                    },
                    dragend: function(map) {
                        //update the search box bounds after dragging the map
                        $scope.searchbox.options.visible = true;
                    }
                }
            },
            searchbox: {
                template: 'searchbox.tpl.html',
                options: {
                    autocomplete: true,
                },
                events: {
                    place_changed: function(autocomplete) {

                        place = autocomplete.getPlace()

                        if (place.address_components) {

                            var marker = {
                                id: place.place_id,
                                coords: {
                                    latitude: place.geometry.location.lat(),
                                    longitude: place.geometry.location.lng()
                                },
                            };


                            $scope.map.center = {
                                latitude: marker.coords.latitude,
                                longitude: marker.coords.longitude
                            };
                            $scope.marker.coords = marker.coords;
                            $scope.marker.id = marker.id;

                        } else {
                            console.log("do something else with the search string: " + place.name);
                        }
                    }
                }
            }
        });
    });
