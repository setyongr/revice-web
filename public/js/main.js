/*  **
Revice AngularJS App Main Script
***/

/* Revice App */
var ReviceApp = angular.module("ReviceApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "satellizer",
    "angular-loading-bar",
    "ngToast",
    "uiGmapgoogle-maps"
]);
// Authentication Config
ReviceApp.config([
    '$authProvider',
    '$provide',
    '$httpProvider',
    'ngToastProvider', function($authProvider, $provide, $httpProvider, ngToastProvider){
  function redirectWhenLoggedOut($q, $injector) {
    return {
      responseError: function(rejection) {
        // Need to use $injector.get to bring in $state or else we get
        // a circular dependency error
        var $state = $injector.get('$state');

        // Check for rejection reason
        var rejectionReason = ['token_not_provided', 'token_expired',
        'token_absent', 'token_invalid'];

        angular.forEach(rejectionReason, function(value) {
          if (rejection.data.error === value) {
            localStorage.removeItem('user');

            $state.go('login');
          }
        });
        return $q.reject(rejection);
      }
    };
  }
  $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

  // Push the new factory into $http interceptor array
  $httpProvider.interceptors.push('redirectWhenLoggedOut');

  $authProvider.loginUrl = 'api/admin/auth';

  $httpProvider.useLegacyPromiseExtensions(true);

  ngToastProvider.configure({
      animation: 'fade', // or 'fade'
      verticalPosition: 'bottom'
    });

}]);
/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
ReviceApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

// Locacati Picker Config
ReviceApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
  GoogleMapApi.configure({
    key: 'AIzaSyCuglouvaHCfA-Tbs28jZ-OmFfM8deEiKU',
    v: '3.17',
    libraries: 'places'
  });
}])
.run(['$templateCache', function ($templateCache) {
  $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" placeholder="Search">');
  $templateCache.put('window.tpl.html', '<div ng-controller="WindowCtrl" ng-init="showPlaceDetails(parameter)">{{place.name}}</div>');
}])

.controller('WindowCtrl', function ($scope) {
  $scope.place = {};
  $scope.showPlaceDetails = function(param) {
    $scope.place = param;
  }
});
/* Setup global settings */
ReviceApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
      assetsPath: '/assets',
      globalPath: '/assets/global',
      layoutPath: '/assets/layouts/layout2'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
ReviceApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
        // Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
ReviceApp.controller('HeaderController', ['$scope', '$auth', '$rootScope', '$state', function($scope, $auth, $rootScope, $state) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });

    var vm = this;
    vm.logout = function() {
      $auth.logout().then(function() {
        //  Remove token from localStorage
        localStorage.removeItem('user');

        $rootScope.authenticated = false;

        $rootScope.currentUser = null;
        $state.go('login');
      });
    };
}]);

/* Setup Layout Part - Sidebar */
ReviceApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);


/* Setup Layout Part - Footer */
ReviceApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
ReviceApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise( function($injector, $location) {
           var $state = $injector.get('$state');
           $state.go('login');
       });

    $stateProvider
        .state('login',{
          url: '/login',
          templateUrl: 'views/login.html',
          controller: 'AuthController',
          controllerAs: 'auth',
          data: {pageTitle: 'Dashboard'},
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'ReviceApp',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                      files: [
                        '/assets/global/plugins/select2/css/select2.min.css',
                        '/assets/global/plugins/select2/css/select2-bootstrap.min.css',
                        '/assets/pages/css/login-5.min.css',
                        'js/controllers/AuthController.js'
                      ]
                  });
              }]
          }
        })
        .state('app',{
          abstract: true,
          templateUrl: 'tpl/layout.html',
        })
        // Dashboard
        .state('app.dashboard', {
            url: "/dashboard",
            templateUrl: "views/dashboard.html",
            controller: "DashboardController",
            data: {pageTitle: 'Dashboard'},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ReviceApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                          '/assets/global/plugins/morris/morris.css',
                          '/assets/global/plugins/morris/morris.min.js',
                          '/assets/global/plugins/morris/raphael-min.js',
                          '/assets/global/plugins/jquery.sparkline.min.js',
                          '/assets/pages/scripts/dashboard.min.js',
                          'js/controllers/DashboardController.js'
                        ]
                    });
                }]
            }
        })
        // Ajax Datetables
        .state('app.userManagement', {
            abstract: true,
            url: "/users",
            template: '<ui-view/>',
            controller: "UserManagementController",
            controllerAs: "user",
            data: {pageTitle: 'User Management'},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ReviceApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'js/services/userFactory.js',
                            'js/controllers/UserManagementController.js'
                        ]
                    });
                }]
            }
        })
        .state('app.userManagement.main', {
          url: "/main",
          templateUrl: "views/user/main.html",
        })
        .state('app.userManagement.edit', {
          url: "/edit/:userId",
          templateUrl: 'views/user/edit.html',
          controller: 'UserManagementEditController',
          controllerAs: 'edit',
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'ReviceApp',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                      files: [
                          'js/controllers/UserManagementEditController.js'
                      ]
                  });
              }]
          }
        })
        .state('app.userManagement.new', {
          url: '/new',
          templateUrl: 'views/user/new.html',
          controller: 'UserManagementNewController',
          controllerAs: 'new',
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'ReviceApp',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                      files: [
                          'js/controllers/UserManagementNewController.js'
                      ]
                  });
              }]
          }
        })
        .state('app.serviceManagement', {
          abstract: true,
          url: "/services",
          template: '<ui-view/>',
          controller: 'ServiceManagementController',
          controllerAs: "service",
          data: {pageTitle: 'Service Management'},
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'ReviceApp',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                      files: [
                        'js/services/serviceFactory.js',
                          'js/controllers/ServiceManagementController.js'
                      ]
                  });
              }]
          }
        })
        .state('app.serviceManagement.main', {
          url: '/main',
          templateUrl: 'views/service/main.html'
        })
        .state('app.serviceManagement.edit', {
          url: "/edit/:userId",
          templateUrl: 'views/service/edit.html',
          controller: 'ServiceManagementEditController',
          controllerAs: 'edit',
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'ReviceApp',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                      files: [
                          'js/controllers/ServiceManagementEditController.js'
                      ]
                  });
              }]
          }
        })
        .state('app.serviceManagement.new', {
          url: '/new',
          templateUrl: 'views/service/new.html',
          controller: 'ServiceManagementNewController',
          controllerAs: 'new',
          resolve: {
              deps: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load({
                      name: 'ReviceApp',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                      files: [
                          'js/controllers/ServiceManagementNewController.js'
                      ]
                  });
              }]
          }
        })
}]);

/* Init global settings and run the app */
ReviceApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view

    $rootScope.$on('$stateChangeStart', function(event, toState) {
      // Parse user to object from localStorage
      var user = angular.fromJson(localStorage.getItem('user'));
      // user is present, its like logged in
      if (user) {
        $rootScope.authenticated = true;
        $rootScope.currentUser = user;

        if (toState.name === 'login') {
          event.preventDefault();

          $state.go('app.dashboard');
        }
      }else{
          $rootScope.authenticated = false;
          if (toState.name !== 'login'){
            event.preventDefault();

            $state.go('login');
          }
      }
    });
}]);
