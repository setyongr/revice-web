(function() {
  'use strict';

angular.module('ReviceApp')
  .factory('userFactory', function($http){
    var userFactory = {};
    userFactory.getUserList = function(pageNumber, count){
      return $http.get('/api/user/?page='+pageNumber+'&limit='+count);
    };

    userFactory.getUser = function(userId){
      return $http.get('/api/user/'+userId);
    };

    userFactory.saveEdit = function(userId, data){
      return $http.put('/api/user/'+userId, data);
    };

    userFactory.delete = function(userId){
      return $http.delete('/api/user/'+userId);
    }
    userFactory.newUser = function(data){
      return $http.post('/api/user/supercreate', data);
    };

    return userFactory;
  });

})();
