(function() {
  'use strict';

angular.module('ReviceApp')
  .factory('serviceFactory', function($http){
    var serviceFactory = {};
    serviceFactory.getServiceList = function(pageNumber, count){
      return $http.get('/api/service/?page='+pageNumber+'&limit='+count);
    };

    serviceFactory.getService = function(serviceId){
      return $http.get('/api/service/'+serviceId);
    };

    serviceFactory.saveEdit = function(serviceId, data){
      return $http.put('/api/service/'+serviceId, data);
    };

    serviceFactory.delete = function(serviceId){
      return $http.delete('/api/service/'+serviceId);
    }
    serviceFactory.newService = function(data){
      return $http.post('/api/service', data);
    };

    serviceFactory.deleteManager = function(serviceId, userId){
        return $http.delete('/api/service/' + serviceId + '/manager/' + userId);
    }

    serviceFactory.newManager = function(serviceId,data){
        return $http.post('/api/service/' + serviceId + '/manager', data);
    }
    return serviceFactory;
  });

})();
