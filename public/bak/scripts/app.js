// public/scripts/app.js

(function() {
  'use strict';

  angular
    .module('authApp', ['ui.router', 'satellizer'])
    .config(function($stateProvider, $urlRouterProvider,
      $authProvider, $httpProvider, $provide) {
      function redirectWhenLoggedOut($q, $injector) {
        return {
          responseError: function(rejection) {
            // Need to use $injector.get to bring in $state or else we get
            // a circular dependency error
            var $state = $injector.get('$state');

            // Check for rejection reason
            var rejectionReason = ['token_not_provided', 'token_expired',
            'token_absent', 'token_invalid'];

            angular.forEach(rejectionReason, function(value) {
              if (rejection.data.error === value) {
                localStorage.removeItem('user');

                $state.go('auth');
              }
            });
            return $q.reject(rejection);
          }
        };
      }

      $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

      // Push the new factory into $http interceptor array
      $httpProvider.interceptors.push('redirectWhenLoggedOut');

      // Satellizer config
      $authProvider.loginUrl = 'api/admin/auth';
      // Redirect to the auth state if any other states
      // are requested other than users
      $urlRouterProvider.otherwise('/auth');

      $stateProvider
        .state('auth', {
          url: '/auth',
          templateUrl: '../views/authView.html',
          controller: 'AuthController as auth'
        })
        .state('users', {
          url: '/users',
          templateUrl: '../views/userView.html',
          controller: 'UserController as user'
        });
    })
    .run(function($rootScope, $state) {
      $rootScope.$on('$stateChangeStart', function(event, toState) {
        // Parse user to object from localStorage
        var user = angular.fromJson(localStorage.getItem('user'));
        // user is present, its like logged in
        if (user) {
          $rootScope.authenticated = true;
          $rootScope.currentUser = user;

          if (toState.name === 'auth') {
            event.preventDefault();

            $state.go('users');
          }
        }
        else{
          if (toStae.name !==)
        }
      });
    });
})();
