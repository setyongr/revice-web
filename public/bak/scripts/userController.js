// public/script/userController.js
(function() {
  'use strict';

  angular
    .module('authApp')
    .controller('UserController', UserController);
    /**
     * [UserController description]
     * @param {[type]} $http      [description]
     * @param {[type]} $auth      [description]
     * @param {[type]} $rootScope [description]
     * @param {[type]} $state     [description]
     */
  function UserController($http, $auth, $rootScope, $state) {
    var vm = this;

    vm.users = '';
    vm.error = '';

    vm.getUsers = function() {
      // This request will hit the index method in the AuthenticateController
      // on the Laravel side and will return the list of users
      $http.get('api/user').success(function(users) {
        vm.users = users;
      }).error(function(error) {
        vm.error = error;
      });
    };

    vm.logout = function() {
      $auth.logout().then(function() {
        //  Remove token from localStorage
        localStorage.removeItem('user');

        $rootScope.authenticated = false;

        $rootScope.currentUser = null;
        $state.go('auth');
      });
    };
  }
})();
