// public/script/authController.js

(function() {
  'use strict';

  angular
    .module('authApp')
    .controller('AuthController', AuthController);
  /**
   * [AuthController description]
   * @param {[type]} $auth      [description]
   * @param {[type]} $state     [description]
   * @param {[type]} $http      [description]
   * @param {[type]} $rootScope [description]
   */
  function AuthController($auth, $state, $http, $rootScope) {
    var vm = this;
    vm.loginError = false;
    vm.loginErrorText = '';

    vm.login = function() {
      var credentials = {
        email: vm.email,
        password: vm.password
      };

      // Use satellizer $auth service to login
      $auth.login(credentials).then(function() {
        vm.loginError = false;
        return $http.get('api/user/current');
      }, function(error) {
        vm.loginError = true;
        vm.loginErrorText = 'Email atau Password salah!';
      }).then(function(response) {
        // Stringlify
        if (!vm.loginError) {
          var user = angular.toJson(response.data.user);

          // Set local storage
          localStorage.setItem('user', user);

          $rootScope.authenticated = true;

          $rootScope.currentUser = response.data.user;

          $state.go('users');
        }
      });
    };
  }
})();
